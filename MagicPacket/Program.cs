﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace MagicPacket
{
    class Program
    {
        static void Main(string[] args)
        {
            String input="";
            if (args.Length>=2 && args[0] == "-a")
            {
                input = args[1];
            }
            else
            {
                System.Console.WriteLine("Magic packet sender\nWho to wake up? (physical address in hex)");

                input = System.Console.ReadLine();
            }

            String matcher = "";
           // for (int i = 0; i < 12; i++)
            {
                matcher += "(?<val>[0-9a-fA-F][0-9a-fA-F])";
                matcher += "(:|-)?";
            }

            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex(matcher);

            if (!reg.IsMatch(input))
            {
                Console.WriteLine("Incorrect phyical address.");
                return;
            }

            
            string res = reg.Replace(input, "${val} ");
            string[] array = res.Split(new string[] {" "}, StringSplitOptions.RemoveEmptyEntries);

            if (array.Length != 6)
            {
                Console.WriteLine("Incorrect phyical address.");
                return;
            }

            System.Console.WriteLine("Sending magic packet:");


            byte[] data=new byte[102];

            System.IO.BinaryWriter wr = new System.IO.BinaryWriter(new System.IO.MemoryStream(data));
            
            for (int i = 0; i < 6; i++)
            wr.Write((byte)0xFF);
            for (int i = 0; i < 16; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    int hex = Int32.Parse(array[j], System.Globalization.NumberStyles.HexNumber);
                    wr.Write((byte)hex);
                }
            }
            System.Net.Sockets.UdpClient client=null;

            try
            {

                foreach (var x in System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces())
                {

                    foreach (var p in x.GetIPProperties().GatewayAddresses)
                    {
                        
                        client = new System.Net.Sockets.UdpClient(p.Address.AddressFamily);
                        client.EnableBroadcast = true;
                        client.Send(data, data.Length, "255.255.255.255", 7);

                    }

                }

                Console.WriteLine("Magic packet has been sent. (may have arrived)");
            }
            catch (Exception ex)
            {

                Console.WriteLine("Magic packet failed to send: " + ex.Message);

            }
            finally
            {
                if (client != null)
                    client.Close();
            }

            wr.Close();

        }
    }
}
